const functions = require('firebase-functions');
const admin = require('firebase-admin');

//////Initialize app///////
//////////////////////////////////////////
admin.initializeApp(functions.config().firebase);



//////Remove a coin after a game played///////
/////////////////////////////////////////////////
exports.removeCoin = functions.database.ref(`/pendingTickets/{uid}/{ticketId}`)
  .onCreate(event => {
    const ticketKey = event.data.key;
    const ticket = event.data.val();
    //I check existing coins
    var ref = admin.database().ref(`userCoins`).child(event.params.uid);

    ref.once("value", function (snapshot) {

      var user = snapshot.val();
      var currentCoins = user.coins;
      //I remove 1 coin
      newCoins = currentCoins - 1;
      admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins);

    }, function (errorObject) {
      console.log("The read failed at checking user coins: " + errorObject.code);
    });
  })


//////Set initial coins for new accounts///////
/////////////////////////////////////////////////
exports.setInitialCoins = functions.auth.user().onCreate(function (event) {

  admin.database().ref(`userCoins`).child(event.data.uid).set({
    coins: 10,
  });

});


//////Remove coins after an exchange///////
/////////////////////////////////////////////////

exports.removeExchangeCoin = functions.database.ref(`/exchanges/{uid}/{exchangeId}`)
  .onCreate(event => {
    const exchangeKey = event.data.key;
    const exchange = event.data.val();

    //I check product cost
    var productCost = admin.database().ref(`exchanges`).child(event.params.uid).child(event.data.key)

    productCost.once("value", function (snapshot) {

      var exchange = snapshot.val();
      exchangeCost = exchange.cost;
      //I check existing coins
      var ref = admin.database().ref(`userCoins`).child(event.params.uid);
      ref.once("value", function (snapshot) {
        var user = snapshot.val();
        var currentCoins = user.coins;
        //I remove the product cost and set the new coins ammount
        newCoins = currentCoins - exchangeCost;
        admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins);
      }, function (errorObject) {
        console.log("The read failed at checking new coins: " + errorObject.code);
      });

    }, function (errorObject) {
      console.log("The read failed at product cost: " + errorObject.code);
    });
  });



//////Check pending Lucky3 tickets///////
/////////////////////////////////////////////////
exports.checkLucky3pendingTickets = functions.database.ref(`/drawLucky3/{draw}/`)
  .onCreate(event => {

    const draw = event.data.val();
    console.log(event.data.val())
    const prize3of3 = 600;
    const prize2of3 = 60;
    const prize1of3 = 6;
    var status;
    var prize;

    console.log('draw.n1 is: ' + draw.n1)
    console.log('draw.n2 is: ' + draw.n2)
    console.log('draw.n3 is: ' + draw.n3)


    var tickets = admin.database().ref(`pendingTickets`).orderByKey();
    //// I check inside pendingTickets and take snapshot
    tickets.once("value", function (snapshot) {
      var users = snapshot.val()

      //// I check inside pendingTickets and take snapshot of each user
      snapshot.forEach(function (childSnapshot) {
        var userTickets = childSnapshot.val();
        var user = childSnapshot.key
        var winnings = 0;
        //// I check inside each user and take snapshot of each ticket
        childSnapshot.forEach(function (childSnapshot) {
          var ticket = childSnapshot.val();
          var ticketKey = childSnapshot.key

          //// I set the cases of the game
          switch (true) {

            //// If 3/3
            case ticket.n1 == draw.n1 && ticket.n2 == draw.n2 && ticket.n3 == draw.n3:
              console.log('x600');
              status = "won";
              prize = prize3of3;
              break;

            //// If 1 & 2
            case ticket.n1 == draw.n1 && ticket.n2 == draw.n2:
              console.log('x60 1&2');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 2 & 3
            case ticket.n3 == draw.n3 && ticket.n2 == draw.n2:
              console.log('x60 2&3');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 1 & 3
            case ticket.n1 == draw.n1 && ticket.n3 == draw.n3:
              console.log('x60 1&3');
              winnings = winnings + prize2of3;
              status = "won";
              prize = prize2of3;
              break;

            //// If 1/3
            case ticket.n1 == draw.n1 || ticket.n2 == draw.n2 || ticket.n3 == draw.n3:
              console.log('x6');
              winnings = winnings + prize1of3;
              status = "won";
              prize = prize1of3;
              break;

            //// If 0/3
            default:
              console.log('LOST')
              status = "lost";
              prize = 0;
          }

          //// I format the checked ticket
          admin.database().ref(`archivedTickets`).child(user).push({
            draw: event.data.key,
            coinsWon: prize,
            createdAt: ticket.createdAt,
            n1: ticket.n1,
            n2: ticket.n2,
            n3: ticket.n3,
            status: status,
            date: ticket.date,
            game: "lucky3",
          }).then(function () {
            admin.database().ref(`pendingTickets`).child(user).child(ticketKey).remove()
          }, function (errorObject) {
            console.log("The remove failed: " + errorObject.code);
          });

        });

        console.log('winnings ' + winnings);

        //// I see the existing coins
        var ref = admin.database().ref(`userCoins`).child(user);
        ref.once("value").then(function (snapshot) {
          var userCoins = snapshot.val();
          var currentCoins = userCoins.coins;
          //I add the winnings and set the new coins ammount
          newCoins = currentCoins + winnings;

          admin.database().ref(`userCoins`).child(user).child(`coins`).set(newCoins)
            .then(function () {
              console.log('new coins inside then: ' + newCoins)
            })
        }, function (errorObject) {
          console.log("The read failed at case 1: " + errorObject.code);
        });

      });
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });
  });



///////////////////////////////////////
//////New Lucky3 Draw///////
/////////////////////////////////////////////////
exports.lucky3draw = functions.https.onRequest((request, response) => {
  var n1;
  var n2;
  var n3;
  var createdAt;

  response.send("HELLO FROM Lucky3draw");
  n1 = Math.floor(Math.random() * 10);
  n2 = Math.floor(Math.random() * 10);
  n3 = Math.floor(Math.random() * 10);
  createdAt = Date.now();
  console.log(n1, n2, n3);
  console.log(createdAt)
  admin.database().ref(`drawLucky3`).push({
    n1: n1,
    n2: n2,
    n3: n3,
    createdAt: createdAt
  })

})


///////////////////////////////////////
//////GET FREE COINS///////
/////////////////////////////////////////////////
exports.getFreeCoins = functions.database.ref(`/rewards/{uid}/{rewardId}`)
  .onCreate(event => {
    const reward = event.data.val();
    const userKey = event.params.uid;

    //// I set the cases of the game
    switch (true) {

      //// ADMOB VIDEO
      case reward.type == "adMobVideo":
        console.log('adMobVideo');
        console.log(event.params.uid);

              //// I see the existing coins
              var ref = admin.database().ref(`userCoins`).child(event.params.uid);
              ref.once("value").then(function (snapshot) {
                var userCoins = snapshot.val();
                var currentCoins = userCoins.coins;
                //I add the winnings and set the new coins ammount
                newCoins = currentCoins + 1;
      
                admin.database().ref(`userCoins`).child(event.params.uid).child(`coins`).set(newCoins)
                  .then(function () {
                    console.log('new coins inside then: ' + newCoins)
                  })
              }, function (errorObject) {
                console.log("The read failed at case 1: " + errorObject.code);
              });
        break;

      //// If default
      default:
        
    }

  })
export interface Product {
    title?: string;
    photo?: string;
    cost?: number;
    description?: string;
    createdAt?: any;
    $key?: string;
    status?: string;
}
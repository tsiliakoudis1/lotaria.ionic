import { Injectable } from '@angular/core';
import { Platform, ToastController, LoadingController, Loading,  } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeRewardVideoConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';
import { User } from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/map';


@Injectable()
export class AdMobService {
  private _admobid;
  private user: User;
  loader: Loading;


  constructor(platform: Platform, private adMobFree: AdMobFree, private toastCtrl: ToastController,
    private database: AngularFireDatabase, private loading: LoadingController, ) {

    console.log('Hello Admob Provider');
    platform.ready().then(() => {
      this._admobid = {};

      if (platform.is('android')) {
        this._admobid = { // for Android
          banner: 'ca-app-pub-3357915166255253/6664362076',
          interstitial: 'ca-app-pub-3357915166255253/7678717383',
          rewardVideo: 'ca-app-pub-3357915166255253/3466395233'
        };
      }

      if (platform.is('ios')) {
        this._admobid = { // for iOS
          banner: 'ca-app-pub-3357915166255253/5035557388',
          interstitial: 'ca-app-pub-3357915166255253/3295100300',
          rewardVideo: 'ca-app-pub-3357915166255253/9588950457'
        };
      }

    });

    document.addEventListener('admob.interstitial.events.CLOSE', (event) => {
    });

    document.addEventListener('admob.rewardvideo.events.CLOSE', (event) => {
      //this.adMobFree.rewardVideo.prepare();
    });


    document.addEventListener('admob.rewardvideo.events.REWARD', (event) => {

      this.database.list(`/rewards/${this.user.uid}/`).push({
        type: "adMobVideo"
      });

      const toast = this.toastCtrl.create({
        message: 'Congratulations! You got a free coin.',
        duration: 4000,
        position: 'middle'
      });
      toast.present();
    });

    document.addEventListener('admob.rewardvideo.events.OPEN', (event) => {
      this.loader.dismiss();
    });

  }


  async showBannerAd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      id: this._admobid.banner,
      isTesting: true,
      autoShow: false,
      //bannerAtTop: true,
    }

    this.adMobFree.banner.config(bannerConfig);

    try {
      const result = this.adMobFree.banner.prepare()
        .then(() => {
          this.adMobFree.banner.show()
        })
        .catch(e => console.log(e));
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

  removeBanner() {
    this.adMobFree.banner.hide();
  }

  async showInterstitial() {
    const interstitialConfig: AdMobFreeInterstitialConfig = {
      id: this._admobid.interstitial,
      isTesting: true,
      autoShow: true,
    }

    this.adMobFree.interstitial.config(interstitialConfig);

    this.adMobFree.interstitial.prepare().then(() => {
      this.adMobFree.interstitial.show();
    })
  }

  async showVideo(user: User) {
    this.user = user;

    const videoRewardsConfig: AdMobFreeRewardVideoConfig = {
      id: this._admobid.rewardVideo,
      isTesting: true,
      autoShow: true,
    }

    this.adMobFree.rewardVideo.config(videoRewardsConfig);

    this.loader = this.loading.create({
      content: 'Preparing your reward.. The video will start in few seconds!',
    }); this.loader.present()
    this.adMobFree.rewardVideo.prepare();
    this.adMobFree.rewardVideo.show();
  }



}
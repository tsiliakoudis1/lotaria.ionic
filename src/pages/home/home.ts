import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { AuthService } from "../../providers/auth/auth.service";
import { DataService } from "../../providers/data/data.service";
import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import {AdMobService} from '../../providers/ad-mob/ad-mob.service'

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {

  coins;
  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  loader: Loading;
 
  
 
  constructor(private navCtrl: NavController, private navParams: NavParams, private data: DataService,
    private adMob: AdMobService, private auth: AuthService,private loading: LoadingController,) {
   
    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })

  }

  navigateToPage(pageName: string) {
    this.navCtrl.push(pageName);
  }

  ionViewWillLoad() {
      this.getCoins();

    console.log('ionViewWillLoad HomePage');
  }

  ionViewDidLoad() {
    //this.adMob.showBannerAd();
}

async getCoins(){
    if (this.authenticatedUser) {
    this.coins = this.data.getCoinsListRef(this.authenticatedUser)
    console.log('async getCoins')
    console.log(this.coins)
    }
  }

  RemoveBanner(){
    this.adMob.removeBanner();
  }

  ShowIntestitial(){
    this.adMob.showInterstitial();
  }


  ShowVideo(){
    if (this.authenticatedUser) {
    this.adMob.showVideo(this.authenticatedUser);

    }
  }

}

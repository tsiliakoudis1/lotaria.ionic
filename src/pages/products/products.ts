import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { DataService } from '../../providers/data/data.service';
import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import { AuthService } from '../../providers/auth/auth.service';
import { Product } from '../../models/product/product.interface';
import * as firebase from 'firebase';



@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  coins;
  productList: {};
  product = {} as Product;

  constructor(private navCtrl: NavController, private navParams: NavParams, private toast: ToastController, 
              private data: DataService,private auth: AuthService, public alertCtrl: AlertController,
              private loadingCtrl: LoadingController, private modal: ModalController) {
    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
  }

  ionViewWillLoad() {
    this.getProducts();
    this.getCoins();
    console.log('ionViewWillLoad ProductsPage');
  }


  async getProducts(){
    this.productList = this.data.getProductListRef()
    }
  
  async getCoins(){
    if (this.authenticatedUser) {
    this.coins = this.data.getCoinsListRef(this.authenticatedUser)
    console.log('this.coins' + this.coins)
    }
  }

  async submitProductExchange(Product) {

    let confirm = this.alertCtrl.create({
      title: 'Exchange coins?',
      message: 'Do you want to exchange ' + Product.cost + ' coins for this '+Product.title+' ?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: async () => {

            let loader = this.loadingCtrl.create({
              content: "Please wait...",
              duration: 3000
            });
            loader.present();
          
            if (this.authenticatedUser) {
              this.product.title = Product.title;
              this.product.description = Product.description;
              this.product.cost = Product.cost;
              this.product.photo = Product.photo; 
              this.product.status = 'Requested';
              this.product.createdAt = firebase.database.ServerValue.TIMESTAMP;
        
              const result = await this.data.submitProductExchange(this.authenticatedUser, this.product)
        
              if (result) {  
              const myModal = this.modal.create('SubmitExchangeModalPage', { 'title': this.product.title, 'cost': Product.cost, 'photo': this.product.photo  });
              setTimeout(() =>  
              myModal.present(),3000);
              }
              else {
                this.toast.create({
                  message: `Something went wrong. Please try again`,
                  duration: 3000
                }).present();
              }
            }


          }
        }
      ]
    });
    confirm.present();
  



  }

}

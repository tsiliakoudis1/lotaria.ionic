import { Component } from '@angular/core';
import { App } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from "../../models/profile/profile.interface";
import { AuthService } from '../../providers/auth/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { User } from 'firebase/app';
import { DataService } from '../../providers/data/data.service';



@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  existingProfile = {} as Profile;
  private authenticatedUser$: Subscription;
  private authenticatedUser: User;
  coins;

  constructor(private auth: AuthService, private navCtrl: NavController, private appCtrl: App,
              private data: DataService, private navParams: NavParams) {

    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;

    })
  }

  ionViewWillLoad() {
    this.getCoins();
    console.log('ionViewWillLoad AccountPage');
  }

  async getCoins(){
    if (this.authenticatedUser) {
    this.coins = this.data.getCoinsListRef(this.authenticatedUser)
    }
  }

  getExistingProfile(profile: Profile) {
    this.existingProfile = profile;
    console.log (this.existingProfile)
  }

 navigateToEditProfilePage(){
this.navCtrl.push('EditprofilePage', {existingProfile: this.existingProfile });
 }

 navigateToPage(pageName){
  this.navCtrl.push(pageName);
   }
 
 signOut() {
   this.auth.signOut
   this.appCtrl.getRootNav().setRoot('LoginPage')
 }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Ticket } from "../../models/ticket/ticket.interface";
import { Subscription } from 'rxjs/Subscription';
import { User } from "firebase/app";
import { DataService } from '../../providers/data/data.service';


/**
 * Generated class for the DrawPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ticket-selection',
  templateUrl: 'ticket-selection.html',
})
export class TicketSelectionPage {


  ticket: Ticket
  draw;
  
  constructor(private navCtrl: NavController, private navParams: NavParams, private data: DataService,) {
    
  }

  ionViewWillLoad() {  
    this.ticket = this.navParams.get('ticket')
    console.log(this.ticket);
    this.getDrawNumbers()
  }


  async getDrawNumbers() {
    
      this.draw = this.data.getLucky3Draw(this.ticket)
      console.log(this.draw.n1)
    
  }


}

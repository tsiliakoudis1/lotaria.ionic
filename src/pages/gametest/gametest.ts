import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GametestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gametest',
  templateUrl: 'gametest.html',
})
export class GametestPage {
  numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  n1: number;
  n2: number;
  n3: number;
   n1display = false
    n2display = false
     n3display = false


  constructor(private navCtrl: NavController, private navParams: NavParams) {
  }

  selectn1(number: number) {
    this.n1display = true;
    this.n1 = (number);
    console.log(this.n1);
  }
  selectn2(number: number) {
    this.n2display = true;
    this.n2 = (number);
    console.log(this.n2);
  }
  selectn3(number: number) {
    this.n3display = true;
    this.n3 = (number);
    console.log(this.n2);
  }
  submit() {
     console.log(this.n1, this.n2, this.n3);
  
  }
}

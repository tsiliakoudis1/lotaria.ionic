import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GametestPage } from './gametest';

@NgModule({
  declarations: [
    GametestPage,
  ],
  imports: [
    IonicPageModule.forChild(GametestPage),
  ],
})
export class GametestPageModule {}

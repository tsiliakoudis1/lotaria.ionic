import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from "../../models/profile/profile.interface";
import { App } from 'ionic-angular';

/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {

  profile = {} as Profile;

  constructor(private navCtrl: NavController, private navParams: NavParams, private appCtrl: App,) {
    this.profile = navParams.get('existingProfile');
  }

saveProfileResult(event: Boolean){
event ? this.appCtrl.getRootNav().setRoot('TabsPage') : console.log("Not authenticated or saved.")
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

}

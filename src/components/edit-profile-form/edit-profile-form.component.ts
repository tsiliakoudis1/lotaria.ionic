import { Component, OnDestroy, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription'
import { User } from 'firebase/app'
import { Profile } from '../../models/profile/profile.interface'
import { DataService } from '../../providers/data/data.service'
import { AuthService } from '../../providers/auth/auth.service'
import * as firebase from 'firebase';
/**
 * Generated class for the EditProfileFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-edit-profile-form',
  templateUrl: 'edit-profile-form.component.html'
})
export class EditProfileFormComponent implements OnInit, OnDestroy {


  private authenticatedUser$: Subscription;
  private authenticatedUser: User;

@Output() saveProfileResult: EventEmitter <Boolean>

  @Input() profile: Profile;


  constructor(private data: DataService, private auth: AuthService) {

    this.saveProfileResult = new EventEmitter<boolean>();

    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
  }

ngOnInit(): void {
  if (!this.profile) {
    this.profile = {} as Profile;
  }
}


////////////////////////////////////////////////
//// Na vgalw to this.profile.coins = 100;/////
////////////////////////////////////////////////

  async saveProfile() {
    if (this.authenticatedUser) {
      this.profile.email = this.authenticatedUser.email;
      this.profile.createdAt = firebase.database.ServerValue.TIMESTAMP;
      const result = await this.data.saveProfile(this.authenticatedUser, this.profile);
      this.saveProfileResult.emit(result);
    }
  }

  ngOnDestroy(): void {
    this.authenticatedUser$.unsubscribe();
  }

}
